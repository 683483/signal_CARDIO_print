import matplotlib.pyplot as plt
import os
# from scipy.fft import fft, fftfreq

import numpy as np
from numpy.fft import fft, ifft

path = "LOGs_señales_muestreadas"

def let_user_pick(options):
    print("Please choose:")

    for idx, element in enumerate(options):
        print("{}) {}".format(idx + 1, element))

    i = input("Enter number: ")
    try:
        if 0 < int(i) <= len(options):
            return int(i) - 1
    except:
        pass
    return None

if __name__ == '__main__':

    files = os.listdir(path)
    while True:
        i = let_user_pick(files)
        # for file in files:
        signalReceived = []
        with open(path + '\\' + files[i]) as f:
            lines = f.readlines()
            for line in lines:
                try:
                    signalReceived.append(int(line, 10))
                except:
                    signalReceived.append(float(line))
        print('HI')

        plt.plot(list(range(len(signalReceived[:]))), signalReceived[:])
        plt.title(f'{files[i]}')
        plt.xlabel('Tiempo')
        plt.ylabel('Tensión [mV]')
        plt.show()



        sr = 501
        # sampling interval
        ts = 1.0 / sr
        t = np.arange(0, 1, ts)

        X = fft(signalReceived[501:500+sr])
        N = len(X)
        n = np.arange(N)
        T = N / sr
        freq = n / T

        plt.figure(figsize=(12, 6))
        plt.subplot(121)

        plt.stem(freq, np.abs(X), 'b', markerfmt=" ", basefmt="-b")
        plt.xlabel('Freq (Hz)')
        plt.ylabel('FFT Amplitude |X(freq)|')
        plt.xlim(0, 10)

        plt.subplot(122)
        plt.plot(t, ifft(X), 'r')
        plt.xlabel('Time (s)')
        plt.ylabel('Amplitude')
        plt.tight_layout()
        plt.show()

        pass

    # with open(f"ECG_FRANCIS_100Hz.txt", 'w', newline='', ) as f:
    #     write = csv.writer(f)
    #     write.writerows([[el[2]] for el in signalReceived[:]])
    # pass

