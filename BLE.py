import asyncio
from bleak import BleakClient
from bleak import BleakScanner
import csv
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np

address = None
signal = []
NUM_MEASURES = 12

def notification_handler(sender, data):
    """Simple notification handler which prints the data received."""
    signal.append(data)
    print("{0}: {1}".format(sender, data))


async def main():
    devices = await BleakScanner.discover()
    print('****************Discovered devices****************')
    for d in devices:
        if d.name == 'SmartInsole':
            address = d.address
        print(d)
    client = BleakClient(address)

    print('****************Conectando****************')
    try:
        await client.connect()
        input("Press Enter to start...")
        await client.start_notify(client.services.characteristics[20].uuid, notification_handler)
        start = datetime.now()
        input("Press Enter to stop...")
        stop = datetime.now()
        pass
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()
    print(start)
    print(stop)
    print(stop-start)
    def bitsTomv(dato, bits_del_dato, rango):
        return dato*rango/(2**bits_del_dato-1)

    ipg_signal = []
    ipg_signal_16bits = []
    ecg_signal = []
    ecg_signal_16bits = []
    int_signal = []
    for measure in signal:
        int_signal.append(int.from_bytes(measure[:-1], 'little', signed=False))
        for i in range(NUM_MEASURES//2):
            ecg_signal.append(bitsTomv(int.from_bytes(measure[i*4+2:i*4+4], 'little', signed=False), 16, 2500))
            ecg_signal_16bits.append(int.from_bytes(measure[i*4+2:i*4+4], 'little', signed=False))
            ipg_signal.append(bitsTomv(int.from_bytes(measure[i*4:i*4+2], 'little', signed=False), 16, 2500))
            ipg_signal_16bits.append(int.from_bytes(measure[i*4:i*4+2], 'little', signed=False))

    # f_signal = input('Indica la frecuencia de la señal: ')
    f_sample = input('Indica la frecuencia de sampleo: ')
    tipo_señal = input('Indica el tipo de señal medida: ')
    filename = input('Indica el nombre del archivo o ENTER para no guardar: ')

    diff = stop-start
    diff = diff.seconds*1000000 + diff.microseconds
    plt.plot(np.linspace(start=0, stop=diff, num=len(ipg_signal)), [el for el in ecg_signal[:]])
    plt.title(f'Señal ECG {tipo_señal} recibida por BLE muestreada a {f_sample}Hz')
    plt.xlabel('Tiempo')
    plt.ylabel('Tensión [mV]')
    if filename != '':
        plt.savefig(f"Graficas_señales_muestreadas\ECG_{filename}.png")
    plt.show()

    plt.plot(np.linspace(start=0, stop=diff, num=len(ipg_signal)), [el for el in ipg_signal[:]])
    plt.title(f'Señal IPG {tipo_señal} recibida por BLE muestreada a {f_sample}Hz')
    plt.xlabel('Tiempo')
    plt.ylabel('Tensión [mV]')
    if filename != '':
        plt.savefig(f"Graficas_señales_muestreadas\IPG_{filename}.png")
    plt.show()

    if filename != '':
        with open(f"LOGs_señales_muestreadas\ECG_{filename}.txt", 'w', newline='', ) as f:
            write = csv.writer(f)
            write.writerows([[el] for el in ecg_signal_16bits])
        with open(f"LOGs_señales_muestreadas\IPG_{filename}.txt", 'w', newline='', ) as f:
            write = csv.writer(f)
            write.writerows([[el] for el in ipg_signal_16bits])
    pass

if __name__ == '__main__':
    asyncio.run(main())
