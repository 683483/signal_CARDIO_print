import asyncio
from bleak import BleakClient
from bleak import BleakScanner
import csv
import matplotlib.pyplot as plt

address = None
signal = []
NUM_MEASURES = 12

def notification_handler(sender, data):
    """Simple notification handler which prints the data received."""
    signal.append(data)
    print("{0}: {1}".format(sender, data))


async def main():
    devices = await BleakScanner.discover()
    print('****************Discovered devices****************')
    for d in devices:
        if d.name == 'CAR-ID2':
            address = d.address
        print(d)
    client = BleakClient(address)

    print('****************Conectando****************')
    try:
        await client.connect()
        input("Press Enter to start...")
        await client.start_notify(client.services.characteristics[13].uuid, notification_handler)
        input("Press Enter to stop...")
        pass
    except Exception as e:
        print(e)
    finally:
        await client.disconnect()

    def bitsTomv(dato, bits_del_dato, rango):
        return dato*rango/(2**bits_del_dato-1)

    mag_X = []
    mag_Y = []
    mag_Z = []
    timestamp_data = []
    mag_X_esp = []
    mag_Y_esp = []
    mag_Z_esp = []
    timestamp_data_esp = []



    ipg_signal = []
    ipg_signal_16bits = []
    ecg_signal = []
    ecg_signal_16bits = []
    int_signal = []

    timestamp_last = 0
    diferencia = 1
    for measure in signal:
        timestamp_last = (int.from_bytes(measure[0:2], 'little', signed=False))
        if len(timestamp_data) != 0:
            diferencia = (timestamp_last - timestamp_data[-1])//10
        if abs(diferencia) > 1:
            for i in range(abs(diferencia)):
                timestamp_data_esp.append(0)
                mag_X_esp.append(0)
                mag_Y_esp.append(0)
                mag_Z_esp.append(0)
        timestamp_data.append(timestamp_last)
        mag_X.append(int.from_bytes(measure[2:4], 'little', signed=True))
        mag_Y.append(int.from_bytes(measure[4:6], 'little', signed=True))
        mag_Z.append(int.from_bytes(measure[6:8], 'little', signed=True))
        timestamp_data_esp.append(timestamp_last)
        mag_X_esp.append(int.from_bytes(measure[2:4], 'little', signed=True))
        mag_Y_esp.append(int.from_bytes(measure[4:6], 'little', signed=True))
        mag_Z_esp.append(int.from_bytes(measure[6:8], 'little', signed=True))
    # f_signal = input('Indica la frecuencia de la señal: ')
    f_sample = input('Indica la frecuencia de sampleo: ')
    tipo_señal = input('Indica el tipo de señal medida: ')
    filename = input('Indica el nombre del archivo o ENTER para no guardar: ')

    plt.plot(list(range(len(mag_X))), [el for el in mag_X[:]], color='r', label='mag_X')
    plt.plot(list(range(len(mag_Y))), [el for el in mag_Y[:]], color='g', label='mag_Y')
    plt.plot(list(range(len(mag_Z))), [el for el in mag_Z[:]], color='b', label='mag_Z')
    plt.title(f'Señal {tipo_señal} recibida por BLE muestreada a {f_sample}Hz')
    plt.xlabel('Tiempo')
    plt.legend(['X', 'Y', 'Z'])
    if filename != '':
        plt.savefig(f"Grafica_magneto\{filename}.png")
    plt.show()

    plt.plot(list(range(len(mag_X_esp))), [el for el in mag_X_esp[:]], color='r', label='mag_X_esp')
    plt.plot(list(range(len(mag_Y_esp))), [el for el in mag_Y_esp[:]], color='g', label='mag_Y_esp')
    plt.plot(list(range(len(mag_Z_esp))), [el for el in mag_Z_esp[:]], color='b', label='mag_Z_esp')
    plt.title(f'Señal {tipo_señal} recibida por BLE muestreada a {f_sample}Hz')
    plt.xlabel('Tiempo')
    plt.legend(['X', 'Y', 'Z'])
    if filename != '':
        plt.savefig(f"Grafica_magneto\{filename}_esp.png")
    plt.show()

    # if filename != '':
    #     with open(f"LOGs_señales_muestreadas\ECG_{filename}.txt", 'w', newline='', ) as f:
    #         write = csv.writer(f)
    #         write.writerows([[el] for el in ecg_signal_16bits])
    #     with open(f"LOGs_señales_muestreadas\IPG_{filename}.txt", 'w', newline='', ) as f:
    #         write = csv.writer(f)
    #         write.writerows([[el] for el in ipg_signal_16bits])
    pass

if __name__ == '__main__':
    asyncio.run(main())
