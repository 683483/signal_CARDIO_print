import csv
import matplotlib.pyplot as plt
from statistics import mean

def Average(lst):
    return mean(lst)

with open('EM4.CSV') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    time = []
    current = []
    for row in csv_reader:
        if line_count != 0:
            time.append(float(row[0]))
            current.append(float(row[1])*100)

        else:
            line_count += 1
    consumo_medio = Average(current)
    consumo_BASE_medio = Average(current)#ADV[90258:113407]; CONN[5331:6500]; MEAS[40400:99453]
    plt.plot(time,current)
    plt.title('Consumo')
    plt.xlabel('Tiempo')
    plt.ylabel('Corriente [mA]')
    plt.show()
    pass